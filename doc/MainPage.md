# SaltyCraft #

A free and open source *Minecraft* clone intended to build a 
modable multiplayer PVP/faction game.

It is based on [Fogleman's Craft](https://github.com/fogleman/Craft).
