
import hashlib

def generate_nonce(length=8):
    """Generate pseudorandom number."""
    return ''.join([str(random.randint(0, 9)) for i in range(length)])

def get_hashsum(str):
    """Returns a sha512 hashsum of for the given string"""
    m = hashlib.sha256()
    m.update(str)
    return m.digest()
