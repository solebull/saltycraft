#ifndef _sign_h_
#define _sign_h_

#define MAX_SIGN_LENGTH 64

/** A sign is an always facing player text */
typedef struct {
  int x;       //!< X position
  int y;       //!< Y position
  int z;       //!< Z position
  int face;
  char text[MAX_SIGN_LENGTH]; //!< The printed text
} Sign;

/** A list of Sign objects
  *
  */
typedef struct {
  unsigned int capacity; //!< The total capacity
  unsigned int size;     //!< The actual size
  Sign *data;            //!< The Sign pointer
} SignList;

void sign_list_alloc(SignList *list, int capacity);
void sign_list_free(SignList *list);
void sign_list_grow(SignList *list);
void sign_list_add(
    SignList *list, int x, int y, int z, int face, const char *text);
int sign_list_remove(SignList *list, int x, int y, int z, int face);
int sign_list_remove_all(SignList *list, int x, int y, int z);

#endif // !_sign_h_
