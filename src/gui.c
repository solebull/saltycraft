#include "gui.h"

#include <GL/gl.h>

#include <stdio.h>

#define OPENGL_ES 1

#define false 0

#if USE_NUKLEAR
#  include <stdlib.h>

// See https://github.com/Immediate-Mode-UI/Nuklear/blob/master/example/canvas.c#L16 for possile values ?
#  define NK_IMPLEMENTATION
#  define NK_INCLUDE_DEFAULT_FONT
#  include "nuklear.h"
#endif

// Trying to use double delta
void (*display_hook)(double);

// Shamelessly stolen from src/SFML/Graphics/GLCheck.cpp.
void
glCheckError(const char* file, unsigned int line, const char* expression)
{
  // Get the last error
  GLenum errorCode = glGetError();

  if (errorCode != GL_NO_ERROR)
    {
      char* error = "Unknown error";
      char* description  = "No description";
      
      // Decode the error code
      switch (errorCode)
        {
	case GL_INVALID_ENUM:
	  {
	    error = "GL_INVALID_ENUM";
	    description = "An unacceptable value has been specified for an enumerated argument.";
	    break;
	  }
	  
	case GL_INVALID_VALUE:
	  {
	    error = "GL_INVALID_VALUE";
	    description = "A numeric argument is out of range.";
	    break;
	  }
	  
	case GL_INVALID_OPERATION:
	  {
	    error = "GL_INVALID_OPERATION";
	    description = "The specified operation is not allowed in the current state.";
	    break;
	  }
	  
	case GL_STACK_OVERFLOW:
	  {
	    error = "GL_STACK_OVERFLOW";
	    description = "This command would cause a stack overflow.";
	    break;
	  }
	  
	case GL_STACK_UNDERFLOW:
	  {
	    error = "GL_STACK_UNDERFLOW";
	    description = "This command would cause a stack underflow.";
	    break;
	  }
	  
	case GL_OUT_OF_MEMORY:
	  {
	    error = "GL_OUT_OF_MEMORY";
	    description = "There is not enough memory left to execute the command.";
	    break;
	  }
	  /*	  
	case GLEXT_GL_INVALID_FRAMEBUFFER_OPERATION:
	  {
	    error = "GL_INVALID_FRAMEBUFFER_OPERATION";
	    description = "The object bound to FRAMEBUFFER_BINDING is not \"framebuffer complete\".";
	    break;
	  }
	  */
        }
      
      // Log the error
      printf("An internal OpenGL call failed in '%s:%d'\n", file, line);
      printf("Expression:\n %s\n", expression);
      printf("Error description:\n %s\n", description);
    }
}


#define glCheck(expr)  do { expr; glCheckError(__FILE__, __LINE__, #expr); } while (false)

void
pushGLStates(void)
{
#ifndef OPENGL_ES
  glCheck(glPushClientAttrib(GL_CLIENT_ALL_ATTRIB_BITS));
  glCheck(glPushAttrib(GL_ALL_ATTRIB_BITS));
#endif
  /*  glCheck(glMatrixMode(GL_MODELVIEW));
  glCheck(glPushMatrix());
  */
  /*  glCheck(glMatrixMode(GL_PROJECTION));
  glCheck(glPushMatrix());
  */
  //  glCheck(glMatrixMode(GL_TEXTURE));
  //glCheck(glPushMatrix());
}

void
resetGLStates(void)
{
  // Define the default OpenGL states
  glCheck(glDisable(GL_CULL_FACE));
  glCheck(glDisable(GL_LIGHTING));
  glCheck(glDisable(GL_DEPTH_TEST));
  glCheck(glDisable(GL_ALPHA_TEST));
  glCheck(glEnable(GL_TEXTURE_2D));
  glCheck(glEnable(GL_BLEND));
  glCheck(glMatrixMode(GL_MODELVIEW));
  glCheck(glLoadIdentity());
  glCheck(glEnableClientState(GL_VERTEX_ARRAY));
  glCheck(glEnableClientState(GL_COLOR_ARRAY));
  glCheck(glEnableClientState(GL_TEXTURE_COORD_ARRAY));
}

void
popGLStates(void)
{
  /*  glCheck(glMatrixMode(GL_PROJECTION));
  glCheck(glPopMatrix());
  */
  /*  glCheck(glMatrixMode(GL_MODELVIEW));
  glCheck(glPopMatrix());
  */
  glCheck(glMatrixMode(GL_TEXTURE));
  glCheck(glPopMatrix());
#ifndef OPENGL_ES
  glCheck(glPopClientAttrib());
  glCheck(glPopAttrib());
#endif
}

struct nk_context ctx;

#define MAX_MEMORY 1000

void
nb_display_hook(double delta)
{
  printf("Inside display_hook\n");
}


void
init_gui(void)
{
#if USE_NUKLEAR
  struct nk_user_font font;
  
  printf("Using nuklear.\n");
  nk_init_fixed(&ctx, calloc(1, MAX_MEMORY), MAX_MEMORY, &font);
#endif  
}
