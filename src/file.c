#include "file.h"

#include <stdio.h>  /* defines FILENAME_MAX */
#include <string.h> // Uses strcat


// #define WINDOWS  /* uncomment this line to use it for windows.*/ 
#ifdef _WIN32
#include <direct.h>
#  define GetCurrentDir _getcwd
#else
#include <unistd.h>
#  define GetCurrentDir getcwd
#endif

char buff[FILENAME_MAX];
char* cwd;

/** Return the current working dir
  *
  */
const char*
get_cwd( void ) {
  cwd = GetCurrentDir( buff, FILENAME_MAX );
  printf("Retuning path CWD : %s\n", cwd);
  return cwd;
}


/** Used for both linux and win32, returns the path to a relative file
  *
  * For linux, it's a relative path ../texture etc...
  * For win32 it's a simple in-zip file
  *
  */
void
get_rel_file(char* path, char* buffer)
{
#ifdef _WIN32
  strcpy(buffer, "./");
#  else  
  strcpy(buffer, "../");
#endif  
  strcat(buffer, path);
}
