#ifndef _LOGGER_H_
#define _LOGGER_H_

// The implementation of this file is splitted in linux/ and win32/

#ifdef _WIN32
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 \
		      : __FILE__)
#endif // !_WIN32

#define l(LEVEL, MSG, ...) _l_impl(__FILENAME__, __LINE__, LEVEL, MSG,	\
				   ##__VA_ARGS__)


#define lassert(LEVEL, MSG) _lassert_impl(__FILENAME__, __LINE__, LEVEL, MSG)

#include <stdbool.h>

typedef enum
  {
    LL_DEBUG=0,
    LL_INFO,
    LL_WARNING,
    LL_ERROR
  }loglevel_t;

void logger_create(loglevel_t);
void logger_destroy();

void _l_impl(const char*, int, loglevel_t, const char*, ...);
void _lassert_impl(const char*, int, bool, const char*);

#endif  // !_LOGGER_H_
