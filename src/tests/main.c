#include <check.h>
#include <stdlib.h>
#include <time.h>

#include "ui_tests.h"
#include "list_tests.h"
#include "logger_tests.h"

int
main()
{
    int number_failed;
    Suite *s;
    SRunner *sr;
    time_t t;

    /* Intializes random number generator */
    srand((unsigned) time(&t));
    
    sr = srunner_create(ui_suite());
    srunner_add_suite(sr, list_suite());
    srunner_add_suite(sr, logger_suite());
    
    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
 }
