#include <check.h>

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "logger_tests.h"

#include "logger.h"

START_TEST (test_logger_create)
{
  logger_create(LL_DEBUG);
  logger_destroy();
  //ck_assert_ptr_ne(l, NULL);
}
END_TEST

START_TEST (test_logger_l)
{
  logger_create(LL_DEBUG);
  l(LL_DEBUG, "Printing a int : %d", 25);
  logger_destroy();
}
END_TEST

Suite* logger_suite(void)
{
  Suite *s;
  TCase *tc_core;
  
  s = suite_create("logger");
  
  /* Core test case */
  tc_core = tcase_create("Core");
  
  tcase_add_test(tc_core, test_logger_create);
  tcase_add_test(tc_core, test_logger_l);
  
  suite_add_tcase(s, tc_core);

  return s;
}

