#include <check.h>

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "list_tests.h"

#include "list.h"

typedef struct
{
  int i;
} int_test;

START_TEST (test_list_create_nonnull)
{
  list_t* l = list_create();
  ck_assert_ptr_ne(l, NULL);
}
END_TEST

START_TEST (test_list_len_zero)
{
  list_t* l = list_create();
  ck_assert_int_eq(list_len(l), 0);
}
END_TEST

START_TEST (test_list_push)
{
  int i = 15;
  list_t* l = list_create();
  list_push(l, &i);
  ck_assert_int_eq(list_len(l), 1);
}
END_TEST

START_TEST (test_list_remove)
{
  int i = 15;
  list_t* l = list_create();
  list_push(l, &i);
  list_remove(l);
  ck_assert_int_eq(list_len(l), 0);
}
END_TEST


START_TEST (test_list_remove_empty)
{
  list_t* l = list_create();
  list_remove(l);
}
END_TEST

// Check we can use list_len() on a previously free'd list because it's used
// in the test_list_dctor_empty which cause a CI segfault
START_TEST (test_list_len_on_free)
{
  list_t* l = list_create();
  free(l);
  ck_assert_int_eq(list_len(l), 0);
}
END_TEST


START_TEST (test_list_dctor_empty)
{
  list_t* l = list_create();
  list_destroy(l);
  // May cause a segfault. Or to prevent this, set l to NULL before
  //  ck_assert_int_eq(list_len(l), 0); 
}
END_TEST

START_TEST (test_list_for_each)
{
  int i = 15, j=12, k=45, m=13; 
  list_t* l = list_create();
  list_push(l, &i);
  list_push(l, &j);
  list_push(l, &k);
  list_push(l, &m);

  int count = 0;
  
  list_t * current = l;
  while (current->next != NULL)
    {
      count++; // here is the callback
      current = current->next;
    }
  
  ck_assert_int_eq(count, 4);
  list_destroy(l);
}
END_TEST

START_TEST (test_list_for_each_macro)
{
  int i = 15, j=12, k=45, m=13; 
  list_t* l = list_create();
  list_push(l, &i);
  list_push(l, &j);
  list_push(l, &k);
  list_push(l, &m);
  
  int count = 0;
  LIST_FOR_EACH(l, item)
  {
    count++;
  }
  LIST_FOR_EACH_END(item)
  list_destroy(l);
  ck_assert_int_eq(count, 4);
  
}
END_TEST

START_TEST (test_list_struct_int)
{
  list_t* l = list_create();
  for (int i = 0; i < 500; ++i)
    {
      int_test* m= malloc(sizeof(int_test));
      m->i = rand() % 50;
      list_push(l, (void*)m);
    }

  int id = 0;
  LIST_FOR_EACH(l, item)
  {
    assert(item); 
    //    assert(item->val); 
    if (item->val)
      {
	int_test* m = (int_test*)item->val;
	assert(m); 
	if (m->i > 50)
	  printf("ID %d: %d > 50", id, m->i);
      }
    ++id;
  }
  LIST_FOR_EACH_END(item)

  list_destroy(l);
}
END_TEST

START_TEST (test_list_foreach_multiple)
{
  int i = 15, j=12, k=45, m=13; 
  list_t* l;

  for (int ii = 0; ii < 500; ++ii)
    {
      l = list_create();
      list_push(l, &i);
      list_push(l, &j);
      list_push(l, &k);
      list_push(l, &m);
      
      int count = 0;
      LIST_FOR_EACH(l, item)
	{
	  count++;
	}
      LIST_FOR_EACH_END(item)
	
	if (count != 4)
	  printf("");
      
      list_destroy(l);
      l = NULL;
    }
}
END_TEST

START_TEST (test_list_second_item)
{
  int i = 15, j=12;
  list_t* l;

  l = list_create();
  list_push(l, &i);
  list_push(l, &j);

  int count = 0;
  LIST_FOR_EACH(l, item)
    {
      count++;
      if (count == 2)
	{
	  int* ret = (int*)item->val;
	  ck_assert_int_eq(*ret, 12);
	}
    }
  LIST_FOR_EACH_END(item)
	
    if (count != 4)
      printf("");
  
  list_destroy(l);
  l = NULL;
}
END_TEST

/** Create the list test suite
  *
  * \return The test suite
  *
  */
Suite* list_suite(void)
{
  Suite *s;
  TCase *tc_core;
  
  s = suite_create("list");
  
  /* Core test case */
  tc_core = tcase_create("Core");
  
  tcase_add_test(tc_core, test_list_create_nonnull);
  tcase_add_test(tc_core, test_list_len_zero);
  tcase_add_test(tc_core, test_list_push);
  tcase_add_test(tc_core, test_list_remove);
  tcase_add_test(tc_core, test_list_remove_empty);
  tcase_add_test(tc_core, test_list_dctor_empty);
  tcase_add_test(tc_core, test_list_for_each);
  tcase_add_test(tc_core, test_list_for_each_macro);
  tcase_add_test(tc_core, test_list_struct_int);
  tcase_add_test(tc_core, test_list_foreach_multiple);
  tcase_add_test(tc_core, test_list_second_item);
  
  suite_add_tcase(s, tc_core);

  return s;
}

