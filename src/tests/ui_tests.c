#include <check.h>

#include <stdbool.h>
#include <stdlib.h>

#include "ui.h"


int count = 0;

void callback_example()
{
  ++count;
}

button_t get_btn()
{
  button_t btn;
  btn.x1 = 100;
  btn.y1 = 100;
  btn.x2 = 150;
  btn.y2 = 150;
  return btn;
}

START_TEST (button_intersect_false)
{
  button_t btn = get_btn();
  ck_assert_int_eq(false, intersect_button(90, 90, &btn));
}
END_TEST

START_TEST (button_intersect_true)
{
  button_t btn = get_btn();
  ck_assert_int_eq(true, intersect_button(110, 110, &btn));
}
END_TEST

START_TEST (button_intersect_far_y)
{
  button_t btn = get_btn();
  ck_assert_int_eq(false, intersect_button(110, 160, &btn));
}
END_TEST

START_TEST (ui_function_ptr)
{
  count  = 0;
  button_cb_t cb;
  cb = callback_example;
  cb();
  
  ck_assert_int_eq(count, 1);
}
END_TEST

START_TEST (ui_function_ptr_inside_struct)
{
  struct test {
    button_cb_t cb;
  };
  count  = 0;

  struct test t;
  
  t.cb = callback_example;
  t.cb();
  
  ck_assert_int_eq(count, 1);
}
END_TEST

START_TEST (ui_function_ptr_inside_struct_ptr)
{
  struct test {
    button_cb_t cb;
  };
  count  = 0;

  struct test* t=malloc(sizeof(struct test));
  
  t->cb = callback_example;
  t->cb();
  
  ck_assert_int_eq(count, 1);
}
END_TEST

Suite* ui_suite(void)
{
  Suite *s;
  TCase *tc_core;
  
  s = suite_create("UI");
  
  /* Core test case */
  tc_core = tcase_create("Core");
  
  tcase_add_test(tc_core, button_intersect_false);
  tcase_add_test(tc_core, button_intersect_true);
  tcase_add_test(tc_core, button_intersect_far_y);
  tcase_add_test(tc_core, ui_function_ptr);
  tcase_add_test(tc_core, ui_function_ptr_inside_struct);
  tcase_add_test(tc_core, ui_function_ptr_inside_struct_ptr);
  suite_add_tcase(s, tc_core);

  return s;
}

