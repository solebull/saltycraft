#include <openssl/sha.h>
#include <stdio.h>
#include <string.h> // USES strlen()

int
usage()
{
  printf("Compute the sha512 hasum of the 1st argument\n\n");
  printf("Usage: sha512-c [string]\n");
  return 1;
}

// Simply compute a SHA512 hashsum to test for C/python compatibility
int
main(int argc, char** argv)
{
  if (argc < 2)
    return usage();
  
  unsigned char* data = argv[1];
  /* If hash is char, openssl will return an  hashsum incompatible 
   * with python one
   */
  unsigned char hash[SHA512_DIGEST_LENGTH];
  char output[SHA512_DIGEST_LENGTH - 1];
  
  SHA512(data, strlen(data), hash);

  for(int i = 0; i < SHA512_DIGEST_LENGTH; ++i)
    sprintf(output + (i * 2), "%02x", hash[i]);

  
  printf("%s", output);
}
