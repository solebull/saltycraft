#/bin/sh

PWD=$1

PY=python2

OUTPUT1=$(./sha512-c $PWD)
OUTPUT2=$($PY ../src/tests/sha512/main.py $PWD)
if [ "${OUTPUT1}" == "${OUTPUT2}" ];
then
    exit 0
else
    echo "${OUTPUT1}"
    echo "${OUTPUT2}"
    exit 1
fi
