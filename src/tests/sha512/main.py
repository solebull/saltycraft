import sys
import hashlib

# Will simply compute a hashsum for the first argument
m = hashlib.sha512()
arg = sys.argv[1].encode('utf-8')
m.update(arg)
print(m.hexdigest())
