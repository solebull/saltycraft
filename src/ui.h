// Global UI functions

#ifndef __UI_H__
#define __UI_H__

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdbool.h>

#include "font.h"
#include "model.h"


// The button callback
typedef void (*button_cb_t)(void);

/** A UI button
  *
  * Keep the top/right and bottom
  *
  */
typedef struct
{
  int x1, y1;
  int x2, y2;
  char text[40];
  bool intersected;
  button_cb_t callback;
} button_t;

void setup_ui(GLFWwindow*, Model*);
void destroy_ui();

int render_ui(Attrib*);
void inject_mouse(int, int);
bool inject_mouse_button(int);
bool intersect_button(int, int, button_t*);
void draw_button(button_t*, Attrib*);

#endif // !__UI_H__
