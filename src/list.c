#include "list.h"

#include <stdlib.h> // For malloc
#include <stddef.h> // for NULL
#include <stdio.h> // for printf

/** Creater a list of void*
  *
  */
list_t*
list_create(void){
  list_t* head = malloc(sizeof(list_t));
  head->next = NULL;
  head->val  = NULL;
  return head;
}

void
list_push(list_t * head, void* val) {
  list_t * current = head;

  while (current->next != NULL)
    {
      current = current->next;
    }

  current->val = val;

    
  /* now we can add a new variable */
  current->next = list_create();
  current->next->next = NULL;
}

/** Get the listr length in number of elements
  *
  * Note: never call this on a freshly destroyed list or you will get
  *       a segfault on the `len++`. If you have to, please be sure to
  *       set head to NULL calling this function.
  *
  * \param head The list's head node.
  *
  * \return The number of element in the list.
  *
  */
int
list_len(list_t* head)
{
  if (head == NULL)
    return 0;

  if (head->val == NULL)
    return 0;
  
  int len = 0;
  list_t* current = head;
  while (current->next != NULL)
    {
      len++;
      current = current->next;
    }
  return len;
}

/** Remove the last item of the list
  *
  */
void
list_remove(list_t *head)
{
    /* get to the second to last node in the list */
    list_t * current = head;
    
    if (current->next == NULL)
      return;
    
    while (current->next->next != NULL)
      current = current->next;

    // now current points to the second to last item of the list, 
    //  so let's remove current->next 
    free(current->next);
    current->next = NULL;
}

/** Destroy and release memory for the given list
  *
  * \param l The list to be destroyed.
  *
  */
void
list_destroy(list_t*l)
{
  for (int i=0; i <= list_len(l); ++i)
    {
      list_remove(l);
    }
  list_remove(l);
  free(l);
}
