#ifndef __GUI_H__
#define __GUI_H__

// GUI helper functions, notably, the opengl states handling ones.
// Mainly comming from src/SFML/Graphics/RenderTarget.hpp source.

/** If any other value than 1, do not use */
#define USE_NUKLEAR 1

void init_gui(void);

// Call them in this order
void pushGLStates(void);
void resetGLStates(void);

// Here call the hook
void nk_display_hook(double delta);

void popGLStates(void);

#endif // !__GUI_H__
