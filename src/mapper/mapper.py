#!/usr/bin/python

# Call it with -i db_file -o png_file

# world (the DLL) comes from ../server/

# sudo apt install python-pil

import os, sys, getopt
sys.path.insert(0, '../server/')

from world import World
import sqlite3

from PIL import Image

def draw(inputfile, outputfile):
    #mypic = Image.new("RGB", (200, 300))
    #    im = Image.open(outputfile)
    #mypic.save(outputfile)
    print(os.path.abspath(inputfile))
    conn = sqlite3.connect(os.path.abspath(inputfile))
    c = conn.cursor()
    c.execute('SELECT (x, y, z) FROM block WHERE(p IS 0 AND q IS 0);')# ORDER BY (Y);')

    print(c.fetchall());
    
def main(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
      print 'test.py -i <inputfile> -o <outputfile>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'mapper.py -i <inputfile> -o <outputfile>'
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
         
   print 'Input file is "', inputfile
   print 'Output file is "', outputfile

   draw(inputfile, outputfile)


   
if __name__ == "__main__":
   main(sys.argv[1:])
