#include "cursor.h"

#include "util.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

// Set the mouse mode up
mouse_mode_t mouse_mode = MM_FREE;
GLuint cursor_texture;

/** Simply return the current mouse_mode */
mouse_mode_t
get_mouse_mode()
{
  return mouse_mode;
}

void
setup_cursor()
{
    glGenTextures(1, &cursor_texture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, cursor_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    load_png_texture("../textures/cursor.png");
}

void draw_cursor()
{
  glUseProgram(0);
  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_SCISSOR_TEST);
  glLogicOp(GL_SET);
  //  glEnable(GL_BLEND);
  //  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, cursor_texture);
  glBegin(GL_QUADS);
  /*
      glTexCoord2i(0, 0); glVertex2i(100, 100);
      glTexCoord2i(0, 1); glVertex2i(100, 500);
      glTexCoord2i(1, 1); glVertex2i(500, 500);
      glTexCoord2i(1, 0); glVertex2i(500, 100);
  */
  glColor3f(1.0, 1.0, 1.0f); glVertex2i(100, 100);
  glColor3f(0.5f, 1.0f, 1.0f); glVertex2i(100, 500);
  glColor3f(1.0f, 1.0f, 0.5f); glVertex2i(500, 500);
  glColor3f(1.0f, 0.8f, 0.1f); glVertex2i(500, 100);

  glEnd();
  //  glDisable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);
  glFlush(); //don't need this with GLUT_DOUBLE and glutSwapBuffers
  //  glEnable(GL_CULL_FACE);
  //  glEnable(GL_DEPTH_TEST);

}


/** Change the current mouse mode
  *
  * This should change the mouse_mode value and set glfw cursor mode.
  *
  */
void
set_mouse_mode(mouse_mode_t mm, GLFWwindow* win)
{
  mouse_mode = mm;
  
  if (MM_FREE)
    glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
  else
    glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}
