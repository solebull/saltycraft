// Implements a basic linked list

#ifndef _LIST_H_
#define _LIST_H_

#include <assert.h>

/** A foreach macro for linked list
  *
  * \param X if the list, Y is the item name
  *
  * Please be sure to use item->val to retrieve your object(not item).
  * You must always check for item->val, sometimes, the last element is NULL.
  *
  */
#define LIST_FOR_EACH(LIST,ITEM)					\
  list_t* ITEM = LIST;					                \
  while (ITEM->next != NULL) { if (ITEM && ITEM->val != NULL) {
#define LIST_FOR_EACH_END(ITEM)  } ITEM=ITEM->next;}

/*
list_t* ITEM = LIST;					                \
while (ITEM->next != NULL) {
  if (ITEM->val != NULL) {
    
  }
  ITEM=ITEM->next;
 }
*/
typedef struct node {
    void* val;
    struct node * next;
} list_t;

list_t* list_create(void);
void    list_destroy(list_t*);

void    list_push(list_t * head, void*);
int     list_len(list_t * head);
void    list_remove(list_t *);

#endif // !_LIST_H_
