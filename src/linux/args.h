#ifndef _ARGS_H_
#define _ARGS_H_

#include <stdbool.h> // Uses bool

#include "model.h" // Uses Model

bool parse_arguments(int argc, char **argv, Model* model);

#endif // !_ARGS_H_
