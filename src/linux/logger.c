#include "logger.h"

#include <stdio.h>  // For printf()
#include <stdlib.h> // For malloc()
#include <assert.h> // For assert()
#include <string.h> // Uses strcat()
#include <stdarg.h> // Uses va_list
#include <assert.h>

#include <time.h>   // Uses time() and localtime()

#include "config.h"

#define FILENAME  "saltycraft.log"

typedef struct
{
  loglevel_t minimum_level;
  FILE*      file;
  struct timespec start_time;
}
  logger_t;

logger_t* logger;

static double TimeSpecToSeconds(struct timespec* ts)
{
  return (double)ts->tv_sec + (double)ts->tv_nsec / 1000000000.0;
}

/** Set the start time of the logger */
void
set_start_time()
{
  if(clock_gettime(CLOCK_MONOTONIC, &logger->start_time))
    {
      assert("Cannot get MONOTONIC time");
    }
}

/** min the level under which, the message is not visible. */
void
logger_create(loglevel_t min)
{
  logger = malloc(sizeof(logger_t));
  set_start_time();

  logger->minimum_level = min;
  logger->file = fopen(FILENAME, "w");
  assert(logger->file && "Can't open logger file");

  fprintf(logger->file, "Welcome to %s (%s)\n", VERSION_STRING, OS);
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  fprintf(logger->file, "Started: %d-%02d-%02d %02d:%02d:%02d\n\n",
	  tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
	  tm.tm_hour, tm.tm_min, tm.tm_sec);


  printf("Welcome to %s (%s)\n\n", VERSION_STRING, OS);
  printf("Started: %d-%02d-%02d %02d:%02d:%02d\n\n",
	 tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
	 tm.tm_hour, tm.tm_min, tm.tm_sec);

}

void
ll_to_str(loglevel_t level)
{
  char g[3];
  if (level == LL_DEBUG)
    sprintf(g,"DD ");
  else if (level == LL_WARNING)
    sprintf(g,"WW ");
  else if (level == LL_INFO)
    sprintf(g,"II ");
  else if (level == LL_ERROR)
    sprintf(g,"EE ");
  else
    sprintf(g,"?? ");

  printf(g);

  fprintf(logger->file, g);

}

void
print_time()
{
  struct timespec end;
  if(clock_gettime(CLOCK_MONOTONIC, &end))
    { /* handle error */ }
  double elapsedSeconds = TimeSpecToSeconds(&end) -
    TimeSpecToSeconds(&logger->start_time);
  printf("T+%08.3f ", elapsedSeconds);
  fprintf(logger->file, "T+%10.3f ", elapsedSeconds);
}

void
print_file(const char* file, int line)
{
  printf("%20s:%-5d ", file, line);
  fprintf(logger->file, "%20s:%-5d ", file, line);
  
}

void
_l_impl(const char* file, int line, loglevel_t level,const char* fmt, ...)
{
  assert(logger && "Logger pointer is NULL");
  if (level >= logger->minimum_level)
    {
      print_time();
      print_file(file, line);
      //log
      char buff[80];
      va_list args;
      va_start(args, fmt);
      vsprintf(buff, fmt, args);
      va_end(args);
      strcat(buff, "\n");

      ll_to_str(level);
      printf(buff);
      fprintf(logger->file, buff);
    }
}

void
logger_destroy()
{
  fclose(logger->file);
  free(logger);
}

void
_lassert_impl(const char* file, int line, bool assertion, const char* message)
{
  
  _l_impl(file, line, LL_DEBUG, "Running assert test for '%s'", message);
  if (!assertion)
    {
      _l_impl(file, line, LL_ERROR, "The following assertion will fail '%s'",
	      message);
      assert(assertion);
    }
}
