# include "args.h"

#include <argtable2.h>  // Uses argtable
#include <string.h>     // Uses strcmp

#include "config.h" // Uses APPNAME
#include "model.h"  // Uses Model

/** Parse command line arguùent
  * 
  * \param argc The number of arguments from main() function.
  * \param argv The arguments array from main() function.
  * \param g    The model used to set arguments values.
  *
  * \returns true if the program must exit after this call.
  *
  */
bool
parse_arguments(int argc, char **argv, Model *g)
{
  // Command line argument handling
  struct arg_lit *help, *version;
  struct arg_str *serverip;
  struct arg_int *serverport;
  struct arg_str *dbpath;
  struct arg_end *end;

  void *argtable[] = {
    help     = arg_litn("hH?", "help", 0, 1, "display this help and exit"),
    version  = arg_litn("vV", "version", 0, 1, "display version info and exit"),
    serverip = arg_strn(NULL, "serverip", "<n>",0, 1,
			"set the multiplayer server IP"),
    serverport = arg_intn(NULL, "serverport", "<n>",0, 1,
			  "set the multiplayer server port"),
    dbpath = arg_strn(NULL, "dbpath", "<n>",0, 1, "set the DB file path"),
    end     = arg_end(20),
  };

  // FIXME: for an unknown reason, -? is seen as an error
  int nerrors = arg_parse(argc,argv,argtable);
  if (nerrors != 0)
    {
      // To quickly fix -? issue, we treat it as help call if first argument
      if (strcmp(argv[1], "-?")==0)
	{
	  printf("Usage: %s", APPNAME);
	  arg_print_syntax(stdout, argtable, "\n");
	  printf("A free minecraft clone.\n\n");
	  arg_print_glossary(stdout, argtable, "  %-25s %s\n");
	  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	  return true;
	}
      else
	{
	  
	  arg_print_errors(stdout,end,"saltycraft");
	  return true;
	}
    }
  
  if (help->count > 0)
    {
      printf("Usage: %s", APPNAME);
      arg_print_syntax(stdout, argtable, "\n");
      printf("A free minecraft clone.\n\n");
      arg_print_glossary(stdout, argtable, "  %-25s %s\n");
      arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
      return true;
    }

    if (version->count > 0)
    {
      printf("%s\n", VERSION);
      arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
      return true;
    }

    if (serverip->count > 0)
    {
      strncpy(g->server_addr, *serverip->sval, MAX_ADDR_LENGTH);
      arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

      g->mode = MODE_ONLINE;
      snprintf(g->db_path, MAX_PATH_LENGTH, "cache.%s.%d.db", serverip->sval,
	       serverport->ival);
      g->mode_changed = 1;

      return false;
    }

    if (serverport->count > 0)
    {
      g->server_port = *serverport->ival;
      arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

      g->mode = MODE_ONLINE;
      snprintf(g->db_path, MAX_PATH_LENGTH, "cache.%s.%d.db", serverip->sval,
	       serverport->ival);
      g->mode_changed = 1;
      
      return false;
    }

    if (dbpath->count > 0)
    {
      strncpy(g->db_path, *dbpath->sval, MAX_ADDR_LENGTH);
      arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

      return false;
    }

}
