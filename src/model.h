#ifndef _MODEL_H_
#define _MODEL_H_

#include <GLFW/glfw3.h>  // Uses GLFWwindow
#include <tinycthread.h> // Uses cnd_t

#include <config.h> // Uses MAX_MESSAGES

#include "map.h"   // Uses Map
#include "sign.h"  // Uses SignList

#define MAX_ADDR_LENGTH 256
#define MAX_TEXT_LENGTH 256
#define MAX_PLAYERS 128
#define MAX_CHUNKS 8192 //!< The max chunk stored in Model.
#define WORKERS 4
#define MAX_NAME_LENGTH 32

#define MAX_PATH_LENGTH 256

#define MODE_OFFLINE 0
#define MODE_ONLINE 1


typedef struct {
    int x;
    int y;
    int z;
    int w;
} Block;

/** A player's state
  *
  * Several of these floats are used as gen_player_buffer() parameters when 
  * generating player. 
  *
  */
typedef struct {
  float x;  //!< X position
  float y;  //!< Y position
  float z;  //!< Z position
  float rx; //!< Used as mat_rotate() parameter
  float ry; //!< Used as mat_rotate() parameter
  float t;  //!< Will store time from glfwGetTime()
} State;

/** A player
  *
  */
typedef struct {
  int id;                     //!< The player's unique id (used in search)
  char name[MAX_NAME_LENGTH]; //!< The player's name
  State state;                //!< 1st player state 
  State state1;               //!< 2nd player state
  State state2;               //!< 3rd player state
  GLuint buffer;              //!< a buffer from gen_player_buffer()
} Player;

/** Defines a map chunk
  *
  * Chunk are stored insinde the Model struct. The max number defined is
  * in MAX_CHUNK.
  *
  */
typedef struct {
    Map map;
    Map lights;
    SignList signs;
    int p;
    int q;
    int faces;
    int sign_faces;
    int dirty;
    int miny;
    int maxy;
    GLuint buffer;
    GLuint sign_buffer;
} Chunk;

/** A Worker item
  *
  * This is used to compute Chunks.
  * Note : Remember when speaking of X,Y,Z that Y is the altitude.
  *
  */
typedef struct {
  int p;          //!< The chunk X position (horizontally)
  int q;          //!< The Chunk Z position (vertically)
  /** Used to know if we need to load maps and data. Set to 0 to avois
   * relooading
    */
  int load;       
  Map *block_maps[3][3]; //!< The blocks data used to generate chunk buffer
  Map *light_maps[3][3]; //!< The blocks data used to generate chunk buffer
  int miny;              //!< Used to determine if the chunk is visible
  int maxy;              //!< Used to determine if the chunk is visible
  int faces;             //!< The number of faces in data
  GLfloat *data;         //!< Teh data used to generate buffer of faces
} WorkerItem;

/** A worker
  *
  * A WORKERS number of workers are created inside Model.
  *
  */
typedef struct {
  int index;          //!< The Worker index (0 < index < WORKERS)
  int state;          //!< One of the WORKER_{IDLE|BUSY|DONE} states
  thrd_t thrd;        //!< The worker thread
  mtx_t mtx;          //!< A mutex
  cnd_t cnd;          //!< A mutex condition
  WorkerItem item;    //!< The associated WorkerItem
} Worker;

typedef struct {
  GLFWwindow *window;
  Worker workers[WORKERS];        //!w A number of Worker
    Chunk chunks[MAX_CHUNKS];
    int chunk_count;
    int create_radius;
    int render_radius;
    int delete_radius;
    int sign_radius;
    Player players[MAX_PLAYERS];
    int player_count;
    int typing;
    char typing_buffer[MAX_TEXT_LENGTH];
    int message_index;
    char messages[MAX_MESSAGES][MAX_TEXT_LENGTH];
    int width;
    int height;
    int observe1;
    int observe2;
    int flying;
    int item_index;
    int scale;
    int ortho;
    float fov;
    int suppress_char;
    int mode;
    int mode_changed;
    char db_path[MAX_PATH_LENGTH];
    char server_addr[MAX_ADDR_LENGTH];
    int server_port;
    int day_length;
    int time_changed;
    Block block0;
    Block block1;
    Block copy0;
    Block copy1;
} Model;

Model* get_model();

#endif // !_MODEL_H_
