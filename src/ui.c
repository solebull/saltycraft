#include "ui.h"

#include <GLFW/glfw3.h>
#include <stdio.h>  // Uses fprintf
#include <stdlib.h> // Uses EXIT_FAILURE
#include <stdarg.h> // Uses va_start/va_end
#include <stddef.h> // Uses sizeof()
#include <string.h> // Uses strcopy()

#include "i18n.h"
#include "list.h"
#include "cursor.h"
#include "logger.h"

/** Defines a to-be-centered dialog 
  *
  * All sizes are in pixels.
  *
  */
typedef struct dialog
{
  int w;         //<! The width of the dialog
  int h;         //<! The height of the dialog
  float r, g, b; //!< The color composants
  float alpha;   //!< the alpha channel value (0.0f = transparent, 1.0f=opaque)
  list_t* button_list; // A list of button
}dialog_t;

/** Contains all status value for the UI */
typedef struct
{
  /** A pointer to the GLFW window to avoid multiple arguments */
  GLFWwindow * window;
  Model*       g;  //!<  The current globalmodel used by this UI instance
  
  int win_width;  //!< The width of the GLWF window in pixels
  int win_height; //!< The height of the GLWF window in pixels
  
  /** True to continue rendering, false to exit game
  *
  */
  bool running;
  /** Switch to true to connect online server */
  bool online;
  /** Should we draw the options dialog ? */
  bool options;
}ui_status_t;

/** The only one dialog in this UI */
dialog_t the_dialog;

// The status
ui_status_t status = {
  .running = true,
  .online  = false,
  .options = false
};

/** Draws a centered dialog
  *
  * Draws a dialog, centered in the screen using 
  *
  */
static void
draw_centered_box(dialog_t* d)
{
  int x = (status.win_width - d->w) / 2;
  int y = (status.win_height - d->h) / 2;

  glBegin(GL_QUADS);
  glColor4f(d->r, d->g, d->b, d->alpha);

  glVertex2i(x, y);
  glVertex2i(x, y + d->h);
  glVertex2i(x + d->w, y + d->h);
  glVertex2i(x + d->w, y);
  
  glVertex2f(0.2f, 0.2f);
  glVertex2f(0.2f, 0.8f);
  glVertex2f(0.8f, 0.8f);
  glVertex2f(0.8f, 0.2f);
  
  glEnd();

}

static void
draw_dialog(int w, int h)
{
  draw_centered_box(&the_dialog);

}

static button_t*
create_button(int button_width, int y, const char* text,
	      button_cb_t cb)
{
  button_t* btn = (button_t*)malloc(sizeof(button_t));
  int x1 = ((status.win_width - button_width) / 2) + 80;
  btn->x1 = x1;
  btn->y1 = y;
  btn->x2 = x1 + button_width;
  btn->y2 = y+20;
  strcpy(btn->text, text);
  btn->callback=cb;
  return btn;
}

/** The GLFW window resize callback
  *
  */
static void
resize_callback(GLFWwindow * win, int width, int height)
{
  status.win_width  = width;
  status.win_height = height;

  // resize buttons
  LIST_FOR_EACH(the_dialog.button_list, item)
    {
      if (item->val)
	{
	  button_t* btn = (button_t*)item->val;
	  int button_width = btn->x2 - btn->x1;
	  int x1 = ((status.win_width - button_width) / 2) + 80;
	  btn->x1 = x1;
	  btn->x2 = x1 + button_width;
	  
	}
    }
  LIST_FOR_EACH_END(item)


}

/** The solo button callback
  *
  */ 
void
on_solo_callback()
{
  set_mouse_mode(MM_LOCKED, status.window);
}

/** The Multiplayer button callback
  *
  */ 
void
on_multi_callback()
{
  status.online = true;

  status.g->mode_changed = 1;
  status.g->mode = MODE_ONLINE;
  strncpy(status.g->server_addr, "127.0.0.1", MAX_ADDR_LENGTH);
  status.g->server_port = 4080;
  snprintf(status.g->db_path, MAX_PATH_LENGTH,
	   "cache.%s.%d.db", "127.0.0.1", 4080);
}

void
on_options_callback()
{
  printf("Options button clicked!\n");
  status.options = true;  
}

void
on_exit_callback()
{
  status.running = false;
}

void
setup_ui(GLFWwindow * win, Model* g)
{
  // Setup the global pointer and get window dimensions
  status.window = win;
  status.g = g;

  glfwGetWindowSize(status.window, &status.win_width, &status.win_height);

  // Set the window resize callback so the global variables are updated
  glfwSetWindowSizeCallback(status.window, &resize_callback);
  
  int border = 2;
  the_dialog.w = 400;
  the_dialog.h = 300;
  the_dialog.r = the_dialog.g = the_dialog.b = 1.0f;
  the_dialog.alpha = 1.0f;

  the_dialog.button_list = list_create();
  int btny = status.win_height-310;
  button_t* b1 = create_button(150, btny, _("Solo"), on_solo_callback);
  b1->callback=on_solo_callback;
  list_push(the_dialog.button_list, (void*)b1);

  button_t* b2 = create_button(150, btny-=50, _("Multiplayer"),
			       on_multi_callback);
  list_push(the_dialog.button_list, (void*)b2);

  button_t* b3 = create_button(150, btny-=50, _("Options"),on_options_callback);
  list_push(the_dialog.button_list, (void*)b3);

  button_t* b4 = create_button(150, btny-=80, _("Exit"), on_exit_callback);
  list_push(the_dialog.button_list, (void*)b4);
  
}

/** Render the UI
  *
  * Must be called once per frame, after the world elements.
  *
  * \return 1 to contuinue rendering, 0 to exit the game, -1 to switch
  *           to online mode. -2 teels options menu is not yet implemented.
  *
  */
int
render_ui(Attrib* text_attrib)
{
  glPushClientAttrib(GL_CLIENT_ALL_ATTRIB_BITS);

  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendEquation(GL_FUNC_ADD);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glLogicOp(GL_SET);
  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  draw_dialog(600, 400);
  
  LIST_FOR_EACH(the_dialog.button_list, item)
    {
      if (item->val)
	draw_button((button_t*)item->val, text_attrib);
    }
  LIST_FOR_EACH_END(item)
  
  glLogicOp(GL_INVERT);
  glDisable(GL_BLEND);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glPopClientAttrib();

  if (status.online)
    {
      set_mouse_mode(MM_LOCKED, status.window);
      return -1;
    }

  if (status.options)
    {
        status.options = false;  
	return -2;
    }

  
  return status.running;
}

void draw_button(button_t* btn, Attrib* text_attrib)
{
  int size = 20;
  
  if (btn->intersected)
    size = 25;
  
  render_text(status.window, text_attrib, ALIGN_CENTER, btn->x1, btn->y2,
	      size, _(btn->text));
}

/** Test if the given x/y (mouse) intercept the given button
  *
  * \return \c true if the mouse intersects the button rectangle shape.
  *
  */
bool
intersect_button(int x, int y, button_t* btn)
{
  btn->intersected = false;

  if (x > btn->x1 && x < btn->x2 && y > btn->y1 && y < btn->y2)
    {
      btn->intersected = true;
    }

  return btn->intersected;
}

/** Inject the mouse into the UI
 * 
 */
void
inject_mouse(int mx, int my)
{

  LIST_FOR_EACH(the_dialog.button_list, item)
    {
      //  draw_button(win, (button_t*)item->val, text_attrib);
      button_t* btn = (button_t*)item->val;
      intersect_button(mx, my, btn);
    }
  LIST_FOR_EACH_END(item)

}

bool
inject_mouse_button(int mouse_button)
{
  if (mouse_button == GLFW_MOUSE_BUTTON_LEFT)
    {
      LIST_FOR_EACH(the_dialog.button_list, item)
	{
	  button_t* btn = (button_t*)item->val;
	  if (btn->intersected)
	    {
	      btn->callback();
	      break;
	    }
	}
      LIST_FOR_EACH_END(item) 
	}	
}


void
destroy_ui()
{
  // Simply avoid further access
  status.window = NULL;
  
  list_destroy(the_dialog.button_list);
}
