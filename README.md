[[_TOC_]]

# Craft

Here is a fork of https://github.com/fogleman/Craft

## Dependencies

	sudo apt install g++ make cmake libglew-dev mesa-common-dev libx11-dev \
	  libxrandr-dev libcurl4-gnutls-dev libxinerama-dev libxcursor-dev \
	  libglfw3-dev check libargtable2-dev 
	  
To be able to cross-compile to win32, add these packages :

	  gcc-mingw-w64-i686 mingw-w64-x86-64-dev

On arch-based linuxes, try :

	sudo pacman -S argtable check

# Building

## Linux

You must build in a subdirectory

	mkdir build
	cd build/
	cmake ..
	make
	./tests
	./saltycraft

If you need to debug unit tests jusing `gdb` :

	cd build/
	gdb ./tests
	(gdb) set environment CK_FORK=no
	run
	bt
	
## Cross compilation to win32

You have to install a hell of dependency in a fixed `~/cross-compil/` directory.

	cd win32/
	./build.sh

# Syncing to original

	git fetch upstream
	git checkout master
	git merge upstream/master

# Command line arguments

	--help                    display this help and exit
	--version                 display version info and exit
	--serverip=<n>            set the multiplayer server IP
	--serverport=<n>          set the multiplayer server port

# Keys

	J - Ortho
	F - Fly
	Tab - Menu
	Esc - Deliver mouse

# Running Server 

## Running manually

	cd src/server
	./server.py

## installation

	cd src/server
	make
	sudo make install
	sudo chown -R <user> /usr/bin/saltycraft
	sudo chown -R <user> /var/log/saltycraft
	sudo update-rc.d saltycraft-server defaults


# Code documentation

	cd build
	doxygen
	
Documentation mainpage is now in *build/html/index.html*.

# Addition to the original

* Changed this README.md
* Now build and run in a subdirectory
* Add a main menu

