#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>

#include "util.h"


typedef struct {
    GLuint program;
    GLuint position;
    GLuint normal;
    GLuint uv;
    GLuint matrix;
    GLuint sampler;
    GLuint camera;
    GLuint timer;
    GLuint extra1;
    GLuint extra2;
    GLuint extra3;
    GLuint extra4;
} Attrib;

int
main()
{

  int window_width = 800;
  int window_height = 600;
  GLFWmonitor *monitor = NULL;

  if (!glfwInit()) {
    return -1;
  }

  GLFWwindow *window = glfwCreateWindow(window_width, window_height,
					"shader test", monitor, NULL);


  glfwWindowHint( GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE );
  glfwMakeContextCurrent(window);
  glfwSwapInterval(VSYNC);
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  /*  
      glfwSetKeyCallback(window, on_key);
      glfwSetCharCallback(window, on_char);
  */
  //  glfwSetMouseButtonCallback(window, on_mouse_button);
  //  glfwSetScrollCallback(window, on_scroll);
  
  if (glewInit() != GLEW_OK) {
    return -1;
  }

  
  GLuint program = load_program("shaders/block_vertex.glsl",
				"shaders/block_fragment.glsl");


  Attrib block_attrib;
    block_attrib.position = glGetAttribLocation(program, "position");
    block_attrib.normal = glGetAttribLocation(program, "normal");
    block_attrib.uv = glGetAttribLocation(program, "uv");
    block_attrib.matrix = glGetUniformLocation(program, "matrix");
    block_attrib.sampler = glGetUniformLocation(program, "sampler");
    block_attrib.extra1 = glGetUniformLocation(program, "sky_sampler");
    block_attrib.extra2 = glGetUniformLocation(program, "daylight");
    block_attrib.extra3 = glGetUniformLocation(program, "fog_distance");
    block_attrib.extra4 = glGetUniformLocation(program, "ortho");
    block_attrib.camera = glGetUniformLocation(program, "camera");
    block_attrib.timer = glGetUniformLocation(program, "timer");

  
  GLenum err = glGetError();
  if (err != GL_NO_ERROR)
    {
      printf("Error '%d'\n", err);
      return 1;
    }
  
}
