#/bin/bash

# Dependencies shoud be in ~/cross-compil/win32

GCC="i686-w64-mingw32-gcc -g"

SRC_FILES=`ls ../src/*.c`
SRC_FILES="$SRC_FILES ../src/win32/logger.c"

SRC_FILES="$SRC_FILES ../deps/tinycthread/tinycthread.c"
SRC_FILES="$SRC_FILES ../deps/noise/noise.c ../deps/lodepng/lodepng.c"
SRC_FILES="$SRC_FILES ../deps/sqlite/sqlite3.c"
SRC_FILES="$SRC_FILES /usr/x86_64-w64-mingw32/lib/libopengl32.a"
SRC_FILES="$SRC_FILES /home/rainbru/cross-compil/win32/curl/lib/libcurl.a"

SRC_FILES="$SRC_FILES  -I/home/rainbru/cross-compil/win64/glew-2.1.0/include/"
CFLAGS="-I../src/ -I../deps/noise/ -I../deps/lodepng/ -I../deps/glew/include/"
CFLAGS="$CFLAGS -I$HOME/cross-compil/win32/glfw/include/ -I../deps/sqlite/"
CFLAGS="$CFLAGS -I$HOME/cross-compil/win32/libintl/include/"
CFLAGS="$CFLAGS -I$HOME/cross-compil/win32/curl/include/ -D_WIN32"
CFLAGS="$CFLAGS -I../deps/tinycthread/ -I../build"

LDFLAGS="-L$HOME/cross-compil/win32/glfw/lib-mingw/ -lglfw3 -lopengl32 -lglu32 -lgdi32"
LDFLAGS="$LDFLAGS -L$HOME/cross-compil/win32/glew-2.1.0/lib/Release/Win32/ -lglew32"
LDFLAGS="$LDFLAGS -L$HOME/cross-compil/win32/curl/lib/ -lcurl"
LDFLAGS="$LDFLAGS -L$HOME/cross-compil/win32/libintl/lib/ -lintl"
LDFLAGS="$LDFLAGS -lws2_32 -lgdi32"

# Bulding resource
i686-w64-mingw32-windres salty.rc -O coff -o salty.res
$GCC icotest.c -o ico-test.exe -lgdi32

# Building binary
$GCC $SRC_FILES -o saltycraft.exe $CFLAGS -lpthread $LDFLAGS
if test $? -eq 0
then
    echo "Building successfull."
else
    exit 1
fi

LIBS="~/cross-compil/win32/lib/libintl.lib"

# Add lglfw3.dll, glew, pthread, curl, openssl
echo "=> Moving content..."
mkdir -p saltycraft
rm -f saltycraft/saltycraft.log
cp saltycraft.exe saltycraft/
cp ~/cross-compil/win32/curl/bin/libcurl.dll saltycraft/
cp ~/cross-compil/win32/glew-2.1.0/bin/Release/Win32/glew32.dll saltycraft/
cp ~/cross-compil/win32/gettext/bin/libintl3.dll saltycraft/libintl3.dll

#cp ~/cross-compil/win32/libiconv2.dll saltycraft/libiconv2.dll

cp ~/cross-compil/win32/gettext/bin/libiconv2.dll saltycraft/libiconv2.dll
cp ~/cross-compil/win32/libiconv2/bin/libiconv2.dll saltycraft/libiconv2.dll
cp ~/cross-compil/win32/vcruntime140.dll saltycraft/vcruntime140.dll

mkdir -p saltycraft/textures
cp ../textures/* saltycraft/textures/
mkdir -p saltycraft/shaders
cp ../shaders/* saltycraft/shaders/

# Install translation
mkdir -p saltycraft/fr/LC_MESSAGES/
cp ../po/mo/fr/saltycraft.mo saltycraft/fr/LC_MESSAGES/

# libcrypto-1_1.dll libssl-1_1.dll
echo "=> Creating zip..."
zip -r saltycraft-win32.zip saltycraft/

md5sum saltycraft-win32.zip > saltycraft-win32.md5

#$GCC file.c ../src/logger.c -o file-test -I ../src/
